/*
 * OpenRISC translation
 *
 *  Copyright (c) 2008-2009 Stuart Brady <stuart.brady@gmail.com>
 *  Copyright (c) 2009 Laurent Desnogues
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */
#include <stdio.h>
#include <stdlib.h>

#include "cpu.h"
#include "disas.h"
#include "tcg-op.h"

typedef struct {
    target_ulong pc;
    target_ulong npc;
    int is_jmp;
    int mem_idx;
    const TranslationBlock *tb;
} DisasContext;

/* global register indexes */

static TCGv_ptr cpu_env;

static TCGv cpu_gpr[32];

static char cpu_reg_names[
    10*3 + 22*4 /* GPR */
];

void openrisc_translate_init(void)
{
    int i;
    char *p;

    cpu_env = tcg_global_reg_new_ptr(TCG_AREG0, "env");

    p = cpu_reg_names;

    for (i = 0; i < 32; i++) {
        sprintf(p, "r%d", i);
        cpu_gpr[i] = tcg_global_mem_new(TCG_AREG0,
                                        offsetof(CPUState, gpr[i]), p);
        p += (i < 10) ? 3 : 4;
    }
}

static inline void store_cpu_offset(TCGv var, int offset)
{
    tcg_gen_st_tl(var, cpu_env, offset);
    tcg_temp_free(var);
}

#define store_cpu_field(var, name) \
    store_cpu_offset(var, offsetof(CPUState, name))

static inline void gen_set_pc_im(target_ulong val)
{
    TCGv tmp = tcg_temp_new();
    tcg_gen_movi_tl(tmp, val);
    store_cpu_field(tmp, pc);
}

static inline void gen_goto_tb(DisasContext *s, int n, target_ulong dest)
{
    const TranslationBlock *tb;

    tb = s->tb;
    if ((tb->pc & TARGET_PAGE_MASK) == (dest & TARGET_PAGE_MASK)) {
        tcg_gen_goto_tb(n);
        gen_set_pc_im(dest);
        tcg_gen_exit_tb((long)tb + n);
    } else {
        gen_set_pc_im(dest);
        tcg_gen_exit_tb(0);
    }
}

static inline uint32_t field(uint32_t val, int start, int length)
{
    val >>= start;
    val &= ~(~0 << length);
    return val;
}

static void disas_openrisc_insn(DisasContext *dc)
{
    uint32_t insn;
    int op, opext, opext2, d, a, b;
    target_ulong im16, im16b;
    TCGv t0;
    int l1, l2;

    insn = ldl_code(dc->pc);
    dc->pc += 4;

    op = field(insn, 26, 6);
    /* opext = field(insn, 21, 5); */
    d = field(insn, 21, 5);
    a = field(insn, 16, 5);
    b = field(insn, 11, 5);
    im16 = field(insn, 0, 16);
    im16b = field(insn, 0, 11) | (d << 11);

    switch (op) {
    case 0x00: /* l.j */
        goto unimp;
    case 0x01: /* l.jal */
        goto unimp;
    case 0x03: /* l.bnf */
        goto unimp;
    case 0x04: /* l.bf */
        goto unimp;
    case 0x05: /* l.nop */
        goto unimp;
    case 0x06:
        /* [l.macrc] */
        /* l.movhi */
        goto unimp;
    case 0x08:
        /* l.sys */
        /* [l.trap] */
        /* csync, msync, psync */
        goto unimp;
    case 0x09: /* l.rfe */
        goto unimp;

#ifdef TARGET_OPENRISC64
    case 0x0a: /* [vector ops] */
        switch (opext) {
        case 0x10: /* lv.all_eq.b */
            goto unimp;
        case 0x11: /* lv.all_eq.h */
            goto unimp;
        case 0x12: /* lv.all_ge.b */
            goto unimp;
        case 0x13: /* lv.all_ge.h */
            goto unimp;
        case 0x14: /* lv.all_gt.b */
            goto unimp;
        case 0x15: /* lv.all_gt.h */
            goto unimp;
        case 0x16: /* lv.all_le.b */
            goto unimp;
        case 0x17: /* lv.all_le.h */
            goto unimp;
        case 0x18: /* lv.all_lt.b */
            goto unimp;
        case 0x19: /* lv.all_lt.h */
            goto unimp;
        case 0x1a: /* lv.all_ne.b */
            goto unimp;
        case 0x1b: /* lv.all_ne.h */
            goto unimp;
        case 0x20: /* lv.any_eq.b */
            goto unimp;
        case 0x21: /* lv.any_eq.h */
            goto unimp;
        case 0x22: /* lv.any_ge.b */
            goto unimp;
        case 0x23: /* lv.any_ge.h */
            goto unimp;
        case 0x24: /* lv.any_gt.b */
            goto unimp;
        case 0x25: /* lv.any_gt.h */
            goto unimp;
        case 0x26: /* lv.any_le.b */
            goto unimp;
        case 0x27: /* lv.any_le.h */
            goto unimp;
        case 0x28: /* lv.any_lt.b */
            goto unimp;
        case 0x29: /* lv.any_lt.h */
            goto unimp;
        case 0x2a: /* lv.any_ne.b */
            goto unimp;
        case 0x2b: /* lv.any_ne.h */
            goto unimp;
        case 0x30: /* lv.add.b */
            goto unimp;
        case 0x31: /* lv.add.h */
            goto unimp;
        case 0x32: /* lv.adds.b */
            goto unimp;
        case 0x33: /* lv.adds.h */
            goto unimp;
        case 0x34: /* lv.addu.b */
            goto unimp;
        case 0x35: /* lv.addu.h */
            goto unimp;
        case 0x36: /* lv.addus.b */
            goto unimp;
        case 0x37: /* lv.addus.h */
            goto unimp;
        case 0x38: /* lv.and */
            goto unimp;
        case 0x39: /* lv.avg.b */
            goto unimp;
        case 0x3a: /* lv.avg.h */
            goto unimp;
        case 0x40: /* lv.cmp_eq.b */
            goto unimp;
        case 0x41: /* lv.cmp_eq.h */
            goto unimp;
        case 0x42: /* lv.cmp_ge.b */
            goto unimp;
        case 0x43: /* lv.cmp_ge.h */
            goto unimp;
        case 0x44: /* lv.cmp_gt.b */
            goto unimp;
        case 0x45: /* lv.cmp_gt.h */
            goto unimp;
        case 0x46: /* lv.cmp_le.b */
            goto unimp;
        case 0x47: /* lv.cmp_le.h */
            goto unimp;
        case 0x48: /* lv.cmp_lt.b */
            goto unimp;
        case 0x49: /* lv.cmp_lt.h */
            goto unimp;
        case 0x4a: /* lv.cmp_ne.b */
            goto unimp;
        case 0x4b: /* lv.cmp_ne.h */
            goto unimp;
        case 0x54: /* lv.madds.h */
            goto unimp;
        case 0x55: /* lv.max.b */
            goto unimp;
        case 0x56: /* lv.max.h */
            goto unimp;
        case 0x57: /* lv.merge.b */
            goto unimp;
        case 0x58: /* lv.merge.h */
            goto unimp;
        case 0x59: /* lv.min.b */
            goto unimp;
        case 0x5a: /* lv.min.h */
            goto unimp;
        case 0x5b: /* lv.msubs.h */
            goto unimp;
        case 0x5c: /* lv.muls.h */
            goto unimp;
        case 0x5d: /* lv.nand */
            goto unimp;
        case 0x5e: /* lv.nor */
            goto unimp;
        case 0x5f: /* lv.or */
            goto unimp;
        case 0x60: /* lv.pack.b */
            goto unimp;
        case 0x61: /* lv.pack.h */
            goto unimp;
        case 0x62: /* lv.packs.b */
            goto unimp;
        case 0x63: /* lv.packs.h */
            goto unimp;
        case 0x64: /* lv.packus.b */
            goto unimp;
        case 0x65: /* lv.packus.h */
            goto unimp;
        case 0x66: /* lv.perm.n */
            goto unimp;
        case 0x67: /* lv.rl.b */
            goto unimp;
        case 0x68: /* lv.rl.h */
            goto unimp;
        case 0x69: /* lv.sll.b */
            goto unimp;
        case 0x6a: /* lv.sll.h */
            goto unimp;
        case 0x6b: /* lv.sll */
            goto unimp;
        case 0x6c: /* lv.srl.b */
            goto unimp;
        case 0x6d: /* lv.srl.h */
            goto unimp;
        case 0x6e: /* lv.sra.b */
            goto unimp;
        case 0x6f: /* lv.sra.h */
            goto unimp;
        case 0x70: /* lv.sra */
            goto unimp;
        case 0x71: /* lv.sub.b */
            goto unimp;
        case 0x72: /* lv.sub.h */
            goto unimp;
        case 0x73: /* lv.subs.b */
            goto unimp;
        case 0x74: /* lv.subs.h */
            goto unimp;
        case 0x75: /* lv.subu.b */
            goto unimp;
        case 0x76: /* lv.subu.h */
            goto unimp;
        case 0x77: /* lv.subus.b */
            goto unimp;
        case 0x78: /* lv.subus.h */
            goto unimp;
        case 0x79: /* lv.unpack.b */
            goto unimp;
        case 0x7a: /* lv.unpack.h */
            goto unimp;
        case 0x7b: /* lv.xor.h */
            goto unimp;
        case 0xc0 ... 0xff: /* lv.cust[1234] */
            goto unimp;
        }
        break;
#endif

    case 0x11: /* l.jr */
        goto unimp;
    case 0x12: /* l.jalr */
        goto unimp;
    case 0x13: /* [l.maci] */
        goto unimp;
    case 0x1c: /* [l.cust1] */
        goto unimp;
    case 0x1d: /* [l.cust2] */
        goto unimp;
    case 0x1e: /* [l.cust3] */
        goto unimp;
    case 0x1f: /* [l.cust4] */
        goto unimp;

    case 0x20 ... 0x26: /* Loads */
        t0 = tcg_temp_new();
        tcg_gen_addi_tl(t0, cpu_gpr[a], (int16_t)im16);
        switch (op) {
#ifdef TARGET_OPENRISC64
        case 0x20: /* l.ld */
            tcg_gen_qemu_ld64(cpu_gpr[d], t0, dc->mem_idx);
            break;
#endif
        case 0x21: /* l.lwz */
            tcg_gen_qemu_ld32u(cpu_gpr[d], t0, dc->mem_idx);
            break;
        case 0x22: /* l.lws */
            tcg_gen_qemu_ld32s(cpu_gpr[d], t0, dc->mem_idx);
            break;
        case 0x23: /* l.lbz */
            tcg_gen_qemu_ld8u(cpu_gpr[d], t0, dc->mem_idx);
            break;
        case 0x24: /* l.lbs */
            tcg_gen_qemu_ld8s(cpu_gpr[d], t0, dc->mem_idx);
            break;
        case 0x25: /* l.lhz */
            tcg_gen_qemu_ld16u(cpu_gpr[d], t0, dc->mem_idx);
            break;
        case 0x26: /* l.lhs */
            tcg_gen_qemu_ld16s(cpu_gpr[d], t0, dc->mem_idx);
            break;
        }
        tcg_temp_free(t0);
        break;

    case 0x27: /* l.addi */
        tcg_gen_addi_tl(cpu_gpr[d], cpu_gpr[a], (int16_t)im16);
        break;
    case 0x28: /* l.addic */
        //tcg_gen_addi_tl(cpu_gpr[d], cpu_gpr[a], im);
        /* FIXME: also adds carry */
        goto unimp;
        break;
    case 0x29: /* l.andi */
        tcg_gen_andi_tl(cpu_gpr[d], cpu_gpr[a], im16);
        break;
    case 0x2a: /* l.ori */
        tcg_gen_ori_tl(cpu_gpr[d], cpu_gpr[a], im16);
        break;
    case 0x2b: /* l.xori */
        tcg_gen_ori_tl(cpu_gpr[d], cpu_gpr[a], im16);
        break;
    case 0x2c: /* l.muli */
        tcg_gen_muli_tl(cpu_gpr[d], cpu_gpr[a], im16);
        break;

    case 0x2d: /* l.mfspr */
        goto unimp;

    case 0x2e:
        switch (opext) {
        case 0x00: /* l.slli */
            // tcg_gen_shli_tl(cpu_gpr[d], cpu_gpr[a], im);
            goto unimp;
        case 0x01: /* l.srli */
            // tcg_gen_shri_tl(cpu_gpr[d], cpu_gpr[a], im);
            goto unimp;
        case 0x02: /* l.srai */
            // tcg_gen_sari_tl(cpu_gpr[d], cpu_gpr[a], im);
            goto unimp;
        case 0x03: /* [l.rori] */
            /* illegal instruction */
            goto unimp;
        }
        break;

    case 0x30: /* l.mtspr */

    case 0x31:
        switch (opext) {
        case 0x01: /* [l.mac] */
            goto unimp;
        case 0x02: /* [l.msb] */
            goto unimp;
        }
        break;

    case 0x32: /* [floating point ops] */
        goto unimp;

    case 0x34 ... 0x37: /* Stores */
        t0 = tcg_temp_new();
        tcg_gen_addi_tl(t0, cpu_gpr[a], (int16_t)im16b);
        switch (op) {
#ifdef TARGET_OPENRISC64
        case 0x34: /* l.sd */
            tcg_gen_qemu_st64(cpu_gpr[b], t0, dc->mem_idx);
            break;
#endif
        case 0x35: /* l.sw */
            tcg_gen_qemu_st32(cpu_gpr[b], t0, dc->mem_idx);
            break;
        case 0x36: /* l.sb */
            tcg_gen_qemu_st8(cpu_gpr[b], t0, dc->mem_idx);
            break;
        case 0x37: /* l.sh */
            tcg_gen_qemu_st16(cpu_gpr[b], t0, dc->mem_idx);
            break;
        }
        tcg_temp_free(t0);
        break;

    case 0x38:
        switch (opext) {
        case 0x0: /* l.add */
            tcg_gen_add_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]);
            break;
        case 0x1: /* l.addc */
            /* tcg_gen_add_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]); */
            /* FIXME: also adds carry */
            goto unimp;
        case 0x2: /* l.sub */
            tcg_gen_sub_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]);
            break;
        case 0x3: /* l.and */
            tcg_gen_and_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]);
            break;
        case 0x4: /* l.or */
            tcg_gen_or_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]);
            break;
        case 0x5: /* l.xor */
            tcg_gen_xor_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]);
            break;
        case 0x6: /* l.mul */
            tcg_gen_mul_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]);
            break;

        case 0x8: /* Shifts */
#ifdef TARGET_OPENRISC64
            /* FIXME: only use bits 0..5 of shift reg */
#else
            /* FIXME: only use bits 0..4 of shift reg */
#endif
            switch (opext2) {
            case 0x0: /* l.sll */
                tcg_gen_shl_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]);
                break;
            case 0x1: /* l.srl */
                tcg_gen_shr_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]);
                break;
            case 0x2: /* l.sra */
                tcg_gen_sar_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]);
                break;
            case 0x3: /* [l.ror] */
                /* tcg_gen_ror_tl(cpu_gpr[d], cpu_gpr[a], cpu_gpr[b]); */
                /* illegal instruction */
                goto unimp;
            }
            break;

        case 0xb: /* l.mulu */
            goto unimp;

        /* optional: */
        case 0x9: /* l.div */
            goto unimp;
        case 0xa: /* l.divu */
            goto unimp;

        case 0xc:
            switch (opext2) {
            case 0x00: /* l.exths */
                tcg_gen_ext16s_tl(cpu_gpr[d], cpu_gpr[a]);
                break;
            case 0x01: /* l.extbs */
                tcg_gen_ext8s_tl(cpu_gpr[d], cpu_gpr[a]);
                break;
            case 0x02: /* l.exthz */
                tcg_gen_ext16u_tl(cpu_gpr[d], cpu_gpr[a]);
                break;
            case 0x03: /* l.extbz */
                tcg_gen_ext8u_tl(cpu_gpr[d], cpu_gpr[a]);
                break;
            }
            break;

        case 0xd:
            switch (opext2) {
#ifdef TARGET_OPENRISC64
            case 0x00: /* l.extws */
                tcg_gen_ext32s_tl(cpu_gpr[d], cpu_gpr[a]);
                break;
            case 0x01: /* l.extwz */
                tcg_gen_ext32u_tl(cpu_gpr[d], cpu_gpr[a]);
                break;
#endif
            }
            break;

        case 0x0e: /* l.cmov */

        case 0xf:
            switch (opext2) {
            case 0x00: /* l.ff1 */
                /* tcg_gen_clz_tl(cpu_gpr[d], cpu_gpr[a]); */
                goto unimp;
            case 0x01: /* l.fl1 */
                /* tcg_gen_ctz_tl(cpu_gpr[d], cpu_gpr[a]); */
                goto unimp;
            }
            break;
        }
        break;

    case 0x2f:
        l1 = gen_new_label();
        l2 = gen_new_label();
        switch (opext) {
        case 0x00: /* l.sfeqi */
            tcg_gen_brcondi_tl(TCG_COND_EQ, cpu_gpr[a], im16, l1);
            break;
        case 0x01: /* l.sfnei */
            tcg_gen_brcondi_tl(TCG_COND_NE, cpu_gpr[a], im16, l1);
            break;
        case 0x02: /* l.sfgtui */
            tcg_gen_brcondi_tl(TCG_COND_GTU, cpu_gpr[a], im16, l1);
            break;
        case 0x03: /* l.sfgeui */
            tcg_gen_brcondi_tl(TCG_COND_GEU, cpu_gpr[a], im16, l1);
            break;
        case 0x04: /* l.sfltui */
            tcg_gen_brcondi_tl(TCG_COND_LTU, cpu_gpr[a], im16, l1);
            break;
        case 0x05: /* l.sfleui */
            tcg_gen_brcondi_tl(TCG_COND_LEU, cpu_gpr[a], im16, l1);
            break;
        case 0x0a: /* l.sfgtsi */
            tcg_gen_brcondi_tl(TCG_COND_GT, cpu_gpr[a], im16, l1);
            break;
        case 0x0b: /* l.sfgesi */
            tcg_gen_brcondi_tl(TCG_COND_GE, cpu_gpr[a], im16, l1);
            break;
        case 0x0c: /* l.sfltsi */
            tcg_gen_brcondi_tl(TCG_COND_LT, cpu_gpr[a], im16, l1);
            break;
        case 0x0d: /* l.sflesi */
            tcg_gen_brcondi_tl(TCG_COND_LE, cpu_gpr[a], im16, l1);
            break;
        }
//      use andi to clear the compare flag
//      tcg_gen_movi_tl(t, 0);
        tcg_gen_br(l2);
        gen_set_label(l1);
//      use ori to set the compare flag
//      tcg_gen_movi_tl(t, 1);
        gen_set_label(l2);
        break;

    case 0x39:
        l1 = gen_new_label();
        l2 = gen_new_label();
        switch (opext) {
        case 0x00: /* l.sfeq */
            tcg_gen_brcond_tl(TCG_COND_EQ, cpu_gpr[a], cpu_gpr[b], l1);
            break;
        case 0x01: /* l.sfne */
            tcg_gen_brcond_tl(TCG_COND_NE, cpu_gpr[a], cpu_gpr[b], l1);
            break;
        case 0x02: /* l.sfgtu */
            tcg_gen_brcond_tl(TCG_COND_GTU, cpu_gpr[a], cpu_gpr[b], l1);
            break;
        case 0x03: /* l.sfgeu */
            tcg_gen_brcond_tl(TCG_COND_GEU, cpu_gpr[a], cpu_gpr[b], l1);
            break;
        case 0x04: /* l.sfltu */
            tcg_gen_brcond_tl(TCG_COND_LTU, cpu_gpr[a], cpu_gpr[b], l1);
            break;
        case 0x05: /* l.sfleu */
            tcg_gen_brcond_tl(TCG_COND_LEU, cpu_gpr[a], cpu_gpr[b], l1);
            break;
        case 0x0a: /* l.sfgts */
            tcg_gen_brcond_tl(TCG_COND_GT, cpu_gpr[a], cpu_gpr[b], l1);
            break;
        case 0x0b: /* l.sfges */
            tcg_gen_brcond_tl(TCG_COND_GE, cpu_gpr[a], cpu_gpr[b], l1);
            break;
        case 0x0c: /* l.sflts */
            tcg_gen_brcond_tl(TCG_COND_LT, cpu_gpr[a], cpu_gpr[b], l1);
            break;
        case 0x0d: /* l.sfles */
            tcg_gen_brcond_tl(TCG_COND_LE, cpu_gpr[a], cpu_gpr[b], l1);
            break;
        }
//      use andi to clear the compare flag
//      tcg_gen_movi_tl(t, 0);
        tcg_gen_br(l2);
        gen_set_label(l1);
//      use ori to set the compare flag
//      tcg_gen_movi_tl(t, 1);
        gen_set_label(l2);
        break;

    case 0x3c: /* [l.cust5] */
        goto unimp;
    case 0x3d: /* [l.cust6] */
        goto unimp;
    case 0x3e: /* [l.cust7] */
        goto unimp;
    case 0x3f: /* [l.cust8] */
        goto unimp;
    }
    return;
 unimp:
    fprintf(stderr, "Unimp @" TARGET_FMT_lx " %08x\n", dc->pc, insn);
    abort();
}

/* generate intermediate code in gen_opc_buf and gen_opparam_buf for
   basic block 'tb'. If search_pc is TRUE, also generate PC
   information for each intermediate instruction. */
/* XXX: tempo */
static inline void gen_intermediate_code_internal(CPUState *env,
                                                  TranslationBlock *tb,
                                                  int search_pc)
{
    DisasContext dc1, *dc = &dc1;
    target_ulong pc_start;

    pc_start = tb->pc;

    dc->tb = tb;

    dc->is_jmp = DISAS_NEXT;
    dc->pc = pc_start;

    do {
        disas_openrisc_insn(dc);
    } while (0);

    switch (dc->is_jmp) {
    case DISAS_NEXT:
        gen_goto_tb(dc, 1, dc->pc);
        break;
    }

    *gen_opc_ptr = INDEX_op_end;

#ifdef DEBUG_DISAS
    if (loglevel & CPU_LOG_TB_IN_ASM) {
        fprintf(logfile, "----------------\n");
        fprintf(logfile, "IN: %s\n", lookup_symbol(pc_start));
        target_disas(logfile, pc_start, dc->pc - pc_start, 0);
        fprintf(logfile, "\n");
    }
#endif

    tb->size = dc->pc - pc_start;
}

void gen_intermediate_code(CPUState *env, TranslationBlock *tb)
{
    gen_intermediate_code_internal(env, tb, 0);
}

void gen_intermediate_code_pc(CPUState *env, TranslationBlock *tb)
{
    gen_intermediate_code_internal(env, tb, 1);
}

/* XXX: tempo */
void cpu_dump_state(CPUState *env, FILE *f,
                    int (*cpu_fprintf)(FILE *f, const char *fmt, ...),
                    int flags)
{
    fprintf(stderr, "pc=" TARGET_FMT_lx "\n", env->pc);
}

/* XXX: tempo */
void gen_pc_load(CPUState *env, TranslationBlock *tb,
                 unsigned long searched_pc, int pc_pos, void *puc)
{
    env->pc = gen_opc_pc[pc_pos];
}
