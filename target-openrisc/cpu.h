/*
 * OpenRISC virtual CPU header
 *
 *  Copyright (c) 2009 Stuart Brady <stuart.brady@gmail.com>
 *  Copyright (c) 2009 Laurent Desnogues
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */
#ifndef CPU_OPENRISC_H
#define CPU_OPENRISC_H

#include "config.h"

#if defined (TARGET_OPENRISC64)
#define TARGET_LONG_BITS 64
#define ELF_MACHINE	EM_NONE
#else
#define TARGET_LONG_BITS 32
#define ELF_MACHINE	EM_OPENRISC
#endif

#define CPUState struct CPUOpenRISCState

#include "cpu-defs.h"

/* XXX: tempo */
#define NB_MMU_MODES 1

/* regs are the general-purpose registers */

struct openrisc_boot_info;

typedef struct CPUOpenRISCState {
    target_ulong gpr[32];

    target_ulong pc;
    target_ulong npc;

    CPU_COMMON

    /* Fields after the common ones are preserved on reset. */
    struct openrisc_boot_info *boot_info;
} CPUOpenRISCState;

CPUOpenRISCState *cpu_openrisc_init(const char *cpu_model);
void openrisc_translate_init(void);
int cpu_openrisc_exec(CPUOpenRISCState *s);
void do_interrupt(CPUOpenRISCState *);

void openrisc_cpu_list(FILE *f, int (*cpu_fprintf)(FILE *f, const char *fmt, ...));

#define TARGET_PAGE_BITS 13

#define cpu_init cpu_openrisc_init
#define cpu_exec cpu_openrisc_exec
#define cpu_gen_code cpu_openrisc_gen_code
#define cpu_signal_handler cpu_openrisc_signal_handler
#define cpu_list openrisc_cpu_list

/* XXX: tempo */
static inline int cpu_mmu_index (CPUState *env)
{
    return 0;
}

#include "cpu-all.h"
#include "exec-all.h"

static inline void cpu_pc_from_tb(CPUState *env, TranslationBlock *tb)
{
    env->pc = tb->pc;
    env->npc = tb->cflags;
}

static inline void cpu_get_tb_cpu_state(CPUState *env, target_ulong *pc,
                                        target_ulong *cs_base, int *flags)
{
    *pc = env->pc;
    *cs_base = env->npc;
    /* XXX: tempo */
    *flags = 0;
}

#endif
